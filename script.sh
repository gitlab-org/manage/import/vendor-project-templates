#!/usr/bin/env bash

export GOPATH=$CI_PROJECT_DIR/.go
mkdir -p $GOPATH

RAND_STR=`openssl rand -hex 3`
DATE_STR=`date +"%F"`
SOURCE_PROJECT="${SOURCE_PROJECT:-ALL}"
BRANCH_NAME=auto-vendor-project-templates-${SOURCE_PROJECT}-${RAND_STR}

git config --global user.name "${GITLAB_USER_NAME}"
git config --global user.email "${GITLAB_USER_EMAIL}"
git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/gitlab-org/gitlab.git

cd gitlab

git checkout -b ${BRANCH_NAME}

. ./scripts/prepare_build.sh
bundle exec ruby -I. -e 'require "config/environment"; TestEnv.init'
echo "Building gitaly"
scripts/gitaly-test-build
echo "Spawning gitaly"
scripts/gitaly-test-spawn
sleep 60
bundle exec rake db:seed_fu

if [ "$SOURCE_PROJECT" == "ALL" ]
then
  bin/rake gitlab:update_project_templates
else
  bin/rake gitlab:update_project_templates[${SOURCE_PROJECT}]
fi

git add vendor/project_templates/*
git commit -m "Automatically vendored project templates"

git push https://lmcandrew:${ACCESS_TOKEN}@gitlab.com/gitlab-org/gitlab.git ${BRANCH_NAME}

BODY="{
    \"id\": \"278964\",
    \"source_branch\": \"${BRANCH_NAME}\",
    \"target_branch\": \"master\",
    \"remove_source_branch\": true,
    \"title\": \"Automatically vendored project templates / ${SOURCE_PROJECT} ${DATE_STR}\",
    \"assignee_id\":\"2629166\",
    \"description\":\"This MR was automatically created by ${CI_PIPELINE_URL}\",
    \"labels\":\"group::import,devops::manage,Category:Templates,backend,feature\"
}";

curl -X POST "https://gitlab.com/api/v4/projects/278964/merge_requests" \
    --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" \
     --header "Content-Type: application/json" \
     --data "${BODY}";

echo "Opened a new MR and assigned to lmcandrew";

