[![pipeline status](https://gitlab.com/gitlab-org/manage/import/vendor-project-templates/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/manage/import/vendor-project-templates/-/commits/master)

This project provides a script that automates the vendoring of [Project Templates](https://gitlab.com/gitlab-org/project-templates). 

The script is designed to be run as part of a GitLab CI Pipeline build. It creates a lightweight GitLab environment, similar to that used in the main [GitLab Project](https://gitlab.com/gitlab-org/gitlab) pipeline. Once the environment is created it calls `rake gitlab:update_project_templates`, commits the changes to a new branch, and finally creates a new Merge Request in the [GitLab Project](https://gitlab.com/gitlab-org/gitlab), which is assigned to @lmcandrew.

Webhooks can be configured in the various Project Template projects to trigger a pipeline build in this project. The webhook should provide a `SOURCE_PROJECT` parameter, to inform the script which project to vendor. If no `SOURCE_PROJECT` parameter is supplied the script will build _all_ projects - this can be achived by manually starting a pipeline.

Example webhook from the [Rails](https://gitlab.com/gitlab-org/project-templates/rails/) project:

`https://gitlab.com/api/v4/projects/24189018/ref/master/trigger/pipeline?token=<TRIGGER-TOKEN>&variables[SOURCE_PROJECT]=rails`

Note: The `SOURCE_PROJECT` variable should match the `name` value of the particular template: https://gitlab.com/gitlab-org/gitlab/-/blob/2c5cf14f2a4191445cf4e90dfb82cfcf9c23e7d0/lib/gitlab/project_template.rb

### TODO

- Update https://about.gitlab.com/community/contribute/project-templates/ to describe new process
- Consider vendoring process for EE templates (note: HIPAA includes Issue data which would currently be removed)
